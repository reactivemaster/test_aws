FROM reactivemaster/virtualenv-base:latest

COPY demo/ /opt/demo/app/

WORKDIR /opt/demo/app

RUN virtualenv --no-site-packages --python=/usr/bin/python3.5 venv && \
venv/bin/pip install -r requirements.txt

EXPOSE 8000

CMD ["venv/bin/python", "manage.py", "runserver", "0.0.0.0:8000"]
