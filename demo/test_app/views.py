from django.http import HttpResponseRedirect
from django.shortcuts import render
from .forms import MyFileForm
from .models import MyFile


def index(request):
	files = MyFile.objects.all()
	return render(request, 'index.html', {'files': files})

def upload(request):
    if request.method == 'POST':
        form = MyFileForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/')
        else:
        	raise Exception(form.errors)
    return render(request, 'upload.html')
