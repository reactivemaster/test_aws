from django.forms import ModelForm
from django.core.exceptions import ValidationError
from .models import MyFile

class MyFileForm(ModelForm):
    class Meta:
        model = MyFile
        fields = ['name', 'file']

    def clean_file(self):
        file = self.cleaned_data.get('file', False)
        if file:
            if file._size > 1024 * 1024:
                raise ValidationError('File too large ( > 1mb )')
            return file
        else:
            raise ValidationError("Couldn't read uploaded file")
